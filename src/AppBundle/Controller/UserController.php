<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Affectation;
use AppBundle\Entity\Quiz;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User as entityUser;
use AppBundle\Entity\Quiz as entityQuiz;
use AppBundle\Repository\QuizRepository;
use AppBundle\Entity\Affectation as entityAffectation;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;
use AppBundle\Form\AffectationType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//use UserBundle\Service\Notify;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Repository\AffectationRepository;
use AppBundle\Entity\Response as entityResponse;
use AppBundle\Entity\Proposition as entityProposition;


use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UserController extends Controller
{

     public function getRoles(){
         return array_flip([
             'admin'=>'ROLE_ADMIN',
             'moderator'=>'ROLE_MODERATOR',
             'candidator'=>'ROLE_CANDIDATOR'
         ]);
     }
    /**
     * @Route("/user/listModerator", name="user_list_moderator")
     */
    public function listModeratorAction(Request $request)
    {
        $repository=$this->getDoctrine()->getRepository(entityUser::class);
        return $this->render('AppBundle:User:listModerator.html.twig',['repository'=>$repository,'roles'=>$this->getRoles()]);
    }

    /**
     * @Route("/user/listCandidator", name="user_list_candidator")
     */
    public function listCandidatorAction(Request $request)
    {
        $repository=$this->getDoctrine()->getRepository(entityUser::class);
        $user=new entityUser();
        $form=$this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

        }
        return $this->render('AppBundle:User:listCandidator.html.twig',[
            'repository'=>$repository,'roles'=>$this->getRoles()
            ,'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/user/addModerator", name="user_add_moderator")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request ,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {

        $em = $this->getDoctrine()->getManager();

        $params=$this->getParameter('mailer');
        $user=new entityUser();
        $form=$this->createForm(UserType::class,$user);
        $form->handleRequest($request);
     //   $encoded = $encoder->encodePassword($user, $form->all()['password']->getData());
        if ($form->isSubmitted() && $form->isValid()) {
                $strln=strlen($form->getData()->getFirstName());
                $password=substr($form->getData()->getFirstName(), -$strln,1).''.$form->getData()->getLastName().(new\DateTime())->format('Y');
                $encoded = $encoder->encodePassword($user, $password);
                $confirmationTocken=md5(uniqid());
                $user->setConfirmationToken($confirmationTocken);
                $username=$form->all()['firstname']->getData().' '.$form->all()['lastname']->getData();
                $em->persist($user->setRoles(['ROLE_MODERATOR'])->setPassword($encoded)->setUsername($username)->setStatus(entityUser::WAITING));
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Ajouter avec succès.'
                );
                $message = (new \Swift_Message($params['username']))
                    ->setSubject('Demande pour modifier votre mot de passe')
                    ->setFrom([$params['email']=>$params['username']])
                    ->setTo($form->all()['email']->getData())
                    ->setBody(
                        $this->renderView(
                            'AppBundle:User:token.html.twig',
                            ['user' => $user,'confirmationTocken'=>$confirmationTocken]
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);

                return $this->redirectToRoute("user_add_moderator");
        }
        return $this->render('AppBundle:User:addModerator.html.twig',['form'=>$form->createView()]);
    }


    /**
     * @Route("/user/addCandidator", name="user_add_candidator")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addCandidatorAction(Request $request ,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {

        $em = $this->getDoctrine()->getManager();
        $params=$this->getParameter('mailer');
        $user=new entityUser();
        $form=$this->createForm(UserType::class,$user);
        $form->handleRequest($request);
       // $encoded = $encoder->encodePassword($user, $form->all()['password']->getData());
       //  if($request->getMethod()=='POST'){
       if ($form->isSubmitted() && $form->isValid()) {
           $file = $user->getBrochure();
           $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
           // moves the file to the directory where brochures are stored
           $file->move(
               $this->getParameter('brochures_directory'),
               $fileName
           );
           $user->setBrochure($fileName);
           $strln=strlen($form->getData()->getFirstName());
           $password=substr($form->getData()->getFirstName(), -$strln,1).''.$form->getData()->getLastName().(new\DateTime())->format('Y');
           $encoded = $encoder->encodePassword($user, $password);
           $confirmationTocken=md5(uniqid());
           $user->setConfirmationToken($confirmationTocken);
         //  $user->setAlias($user->getFirstName());
           $username=$form->all()['firstname']->getData().' '.$form->all()['lastname']->getData();
          // die();
           // $em->persist($user->setAlias($form->all()['lastname']->getData())->setRoles(['ROLE_CANDIDATOR'])->setPassword($encoded)->setUsername($username)->setStatus(entityUser::WAITING));
           $em->persist($user->setRoles(['ROLE_CANDIDATOR'])->setPassword($encoded)->setUsername($username)->setStatus(entityUser::WAITING));

           $em->flush();

           $this->get('session')->getFlashBag()->add(
                'notice',
                'Ajouter avec succès.'
            );
            $message = (new \Swift_Message($params['username']))
                ->setSubject('Demande pour modifier votre mot de passe')
                ->setFrom([$params['email']=>$params['username']])
                ->setTo($form->all()['email']->getData())
                ->setBody(
                    $this->renderView(
                        'AppBundle:User:token.html.twig',
                        ['user' => $user,'confirmationTocken'=>$confirmationTocken]
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);

            return $this->redirectToRoute("user_add_candidator");
        }
        return $this->render('AppBundle:User:addCandidator.html.twig',['form'=>$form->createView()]);
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }



    /**
     * @Route("/user/{confirmationTocken}/finishRegistration", name="finish_registration")
     * @param $confirmationTocken
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function finishRegistrationAction($confirmationTocken,Request $request){

        $em=$this->getDoctrine()->getManager();
        $user=$this->getDoctrine()->getRepository(entityUser::class)->findOneBy(['confirmationToken'=>$confirmationTocken,'status'=>entityUser::WAITING]);
        if(empty($user)){
            return new \Symfony\Component\HttpFoundation\Response("Désolé votre mot de passe a été déjà modifier");
        }
        if($request->getMethod()=="POST"){
            if(($pwd=$request->request->get('password'))!=""){
                $userManager=$this->get('fos_user.user_manager');
                $user->setPlainPassword($pwd)->setStatus(entityUser::UPDATING);
                $userManager->updateUser($user);
                return new \Symfony\Component\HttpFoundation\Response("Votre mot de passe a été bien mise à jour");
            }
        }
        return $this->render('AppBundle:User:finishRegistration.html.twig');

    }

    /**
     * @Route("/user/{id}/updateModerator",name="user_update_moderator")
     * @param $id
     * @param Request $request
     */
    public function updateModeratorAction($id,Request $request){

        $em=$this->getDoctrine()->getManager();
        $forms=$request->request->all();
        $user=$this->getDoctrine()->getRepository(entityUser::class)->find($id);
        $password=$user->getPassword();
        if($request->getMethod()=="POST"){
            $em->persist($user
                ->setFirstName($forms['first_name'])
                ->setLastName($forms['last_name'])
                ->setPassword($password)
                ->setEmail($forms['email'])
                ->setPhone($forms['phone'])
                ->setUserName($forms['first_name'].' '.$forms['last_name'])
                //->setRoles([$forms['roles']])
            );
            $em->flush();
            return $this->redirectToRoute("user_list_moderator");
        }
    }

    /**
     * @Route("/user/{id}/updateCandidator",name="user_update_candidator")
     * @param $id
     * @param Request $request
     */
    public function updateCandidatorAction($id,Request $request){

         $em=$this->getDoctrine()->getManager();
         $user=$this->getDoctrine()->getRepository(entityUser::class)->find($id);
         $password=$user->getPassword();
       //  $brochure=$user->getBrochure();
         $form=$this->createForm(UserType::class,$user);
         $form->handleRequest($request);
         if($request->getMethod()=='POST'){
             $file = $user->getBrochure();
             $brochure = $this->generateUniqueFileName().'.'.$file->guessExtension();
             $file->move(
                 $this->getParameter('brochures_directory'),
                 $brochure
             );
            $em->persist($user
                ->setFirstName($request->request->all()['userbundle_user']['firstname'])
                ->setLastName($request->request->all()['userbundle_user']['lastname'])
                ->setEmail($request->request->all()['userbundle_user']['email'])
                ->setUserName($request->request->all()['userbundle_user']['firstname'].' '.$request->request->all()['userbundle_user']['lastname'])
                ->setRoles(['ROLE_CANDIDATOR'])
                ->setEnabled(entityUser::STATUS_AVAIBLE)
                ->setPassword($password)
                ->setBrochure($brochure)
            );
            $em->flush();
            return $this->redirectToRoute("user_list_candidator");
        }
        return $this->render('AppBundle:User:edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route ("/user/{id}/disabledModerator" , name="user_disabled_moderator")
     * @return int
     */
    public function disabledOrEnabledModeratorAction($id) {

        $em=$this->getDoctrine()->getManager();
        $repository=$this->getDoctrine()->getRepository(entityUser::class)->find($id);
        $repository=$repository->isEnabled()==entityUser::STATUS_AVAIBLE?
            $repository->setEnabled(entityUser::STATUS_NOTAVAIBLE):
            $repository->setEnabled(entityUser::STATUS_AVAIBLE);
        $em->persist($repository);
        $em->flush();
        return $this->redirectToRoute("user_list_moderator");
    }


    /**
     * @Route ("/user/{id}/disabledCandidator" , name="user_disabled_candidator")
     * @return int
     */
    public function disabledOrEnabledCandidatorAction($id) {

        $em=$this->getDoctrine()->getManager();
        $repository=$this->getDoctrine()->getRepository(entityUser::class)->find($id);
        $repository=$repository->isEnabled()==entityUser::STATUS_AVAIBLE?
            $repository->setEnabled(entityUser::STATUS_NOTAVAIBLE):
            $repository->setEnabled(entityUser::STATUS_AVAIBLE);
        $em->persist($repository);
        $em->flush();
        return $this->redirectToRoute("user_list_candidator");
    }

    /**
     * @Route("/user/{id}/affectation",name="user_affectation")
     * @param $id
     */
    public function affectationAction(Request $request,$id,\Swift_Mailer $mailer){

        $em=$this->getDoctrine()->getManager();
        $params=$this->getParameter('mailer');
        $repQuiz=$this->getDoctrine()->getRepository(entityQuiz::class);
        $user=$this->getDoctrine()->getRepository(entityUser::class )->find($id);
        $affectation=new entityAffectation();
        $form=$this->createForm(AffectationType::class,$affectation);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach($request->get('quiz') as $idQ){
                $affectation=new entityAffectation();
                $affectation->setQuiz($repQuiz->find($idQ));
                $affectation->setStatus(entityAffectation::STATUS_WAITING);
                $affectation->setUser($user);
                $em->persist($affectation);
            }
            $em->flush();

            $message = (new \Swift_Message($params['username']))
                ->setSubject('Demande de test en ligne.')
                ->setFrom([$params['email']=>$params['username']])
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        'AppBundle:User:quiz.html.twig',
                        ['user' => $user]
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);
            $this->get('session')->getFlashBag()->add(
                'notice',
                'Ajouter avec succès.'
            );
            return $this->redirectToRoute("user_affectation",['id'=>$id]);
        }

        return $this->render('AppBundle:User:affectation.html.twig',['user'=>$user,'repQuiz'=>$repQuiz,'form'=>$form->createView()]);
    }

    /**
     * @Route("/affectation/delete", name="affectation_delete")
     * @param Request $request
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getEntityManager();
        $affectations=$em->getRepository(entityAffectation::class)->findById($request->get('id'));
        foreach($affectations as $aff){
            $em->remove($aff);
            $em->flush();
        }
        return new Response(json_encode([$request->get('id')]), 200, ['Content-Type' => 'application/json'] );
    }

    /**
     * @Route("/user/{idUser}/quiz/{idQuiz}",name="quiz_start")
     * @param Request $request
     * @param $idUser
     * @param $idQuiz
     * @return Response
     */
    public function startQuizAction(Request $request,$idUser,$idQuiz){
        $quiz=$this->getDoctrine()->getRepository(entityQuiz::class)->find($idQuiz);
        return $this->render('AppBundle:User:startQuiz.html.twig',
            ['quiz'=>$quiz,'idUser'=>$idUser]
        );
    }
    /**
     * @Route("/user/{idUser}/quiz/{idQuiz}/check",name="quiz_check")
     * @param Request $request
     * @param $idUser
     * @param $idQuiz
     * @return Response
     */
    public function checkQuizAction(Request $request,$idUser,$idQuiz){

        $em=$this->getDoctrine()->getManager();
        $repA=$this->getDoctrine()->getRepository(Affectation::class);
        $repU=$this->getDoctrine()->getRepository(User::class);
        $repQ=$this->getDoctrine()->getRepository(Quiz::class);
        $repP=$this->getDoctrine()->getRepository(entityProposition::class);
        $user=$repU->find($idUser);
        if($request->getMethod()=="POST"){
            $entity=$repA->findOneBy(['user'=>$repU->find($idUser),'quiz'=>$repQ->find($idQuiz)]);
            $entity->setStatus(Affectation::STATUS_TREATY);
            $em->persist($entity);
            $em->flush();

            for($i=1;$i<=20;$i++){
                if($request->get('unique_response_'.$i)){
                    $response=new entityResponse();
                    $response->setUser($user);
                    $response->setProposition($repP->find($request->get('unique_response_'.$i)[0]));
                    $em->persist($response);
                    $em->flush();
                }
            }

            if($request->get('multiple_response')){
                foreach($request->get('multiple_response') as $multiple){
                    $response=new entityResponse();
                    $response->setUser($user);
                    $response->setProposition($repP->find($multiple));
                    $em->persist($response);
                    $em->flush();
                }
            }

            if($repA->findBy(['status'=>Affectation::STATUS_WAITING])){
                  return $this->redirectToRoute('homepage');
            }else{
                $user=$repU->find($idUser);
                $user->setEnabled(User::STATUS_NOTAVAIBLE);
               $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('fos_user_security_logout');
            }
        }
      //  die();
    }


    /**
     * @Route("/user/{idUser}/quiz/{idQuiz}/consulter",name="quiz_consulter")
     * @param Request $request
     * @param $idUser
     * @param $idQuiz
     */
    public function consulterAction (Request $request,$idUser,$idQuiz){

        $repositoryQuiz=$this->getDoctrine()->getRepository(entityQuiz::class);
        $user=$this->getDoctrine()->getRepository(entityUser::class)->find($idUser);
        $responses=$this->getDoctrine()->getRepository(entityResponse::class)->findBy(['user'=>$idUser]);
       // dump($repositoryQuiz->sumNote($user,$repositoryQuiz->find($idQuiz)));
      //  die();
        return $this->render('AppBundle:User:consulter.html.twig',
            ['repositoryQuiz'=>$repositoryQuiz,
                'responses'=>$responses,
                'user'=>$user,'idQuiz'=>$idQuiz
            ]
        );
    }

    public function notifyAction(){
        $affectation=$this->getDoctrine()->getRepository(entityAffectation::class)->findBy(['status'=>entityAffectation::STATUS_TREATY]);
        return $this->render('AppBundle:user:notify.html.twig',['affectation'=>$affectation]);
    }

    /**
     * @Route("/notify",name="all_notify")
     */
    public function allNotifyAction(){

        $affectation=$this->getDoctrine()->getRepository(entityAffectation::class)->findBy(['status'=>entityAffectation::STATUS_TREATY]);
        return $this->render('AppBundle:user:allNotify.html.twig',['affectation'=>$affectation]);
    }

    /**
     * @Route("/notify/details/{idA}/{idUser}/{idQuiz}",name="details_notify")
     */
    public function detailsNotifyAction($idA,$idUser,$idQuiz){
        $em=$this->getDoctrine()->getManager();
        $affectation=$this->getDoctrine()->getRepository(entityAffectation::class)->find($idA);
        $affectation->setStatus(entityAffectation::STATUS_TREATY_NOTIFY);
        $em->persist($affectation);
        $em->flush();
        return $this->redirectToRoute('quiz_consulter',['idUser'=>$idUser,'idQuiz'=>$idQuiz]);
    }

    /**
     * @Route("/download/{id}",name="download_cv")
     */
    public function downloadCvAction($id){

        $user=$this->getDoctrine()->getRepository(entityUser::class)->find($id);
        $publicResourcesFolderPath =  $this->getParameter('brochures_directory').'/';
        $filename = $user->getBrochure();
        $response = new BinaryFileResponse($publicResourcesFolderPath.$filename);
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        if($mimeTypeGuesser->isSupported()){
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($publicResourcesFolderPath.$filename));
        }else{
            $response->headers->set('Content-Type', 'text/plain');
        }
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;

    }
}
