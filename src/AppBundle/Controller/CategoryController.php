<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Category as entityCategory;
use AppBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class CategoryController extends Controller
{
    /**
     * @Route("/category/list", name="category_list")
     */
    public function listAction()
    {
        $repository=$this->getDoctrine()->getRepository(entityCategory::class);
        return $this->render('AppBundle:category:list.html.twig',
            ['repository'=>$repository]
        );
    }

    /**
     * @Route("/category/add", name="category_add")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category=new entityCategory();
        $form = $this->createForm( CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $em->persist($category);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                'Ajouter avec succès.'
            );
            return $this->redirectToRoute("category_add");
        }
        return $this->render('AppBundle:category:add.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("/category/update/{id}", name="category_update")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $category=$em->getRepository(entityCategory::class)->find($id);
            if ($request->getMethod() == 'POST') {
                $category->setName($request->request->get('name'));
                $em->persist($category);
                $em->flush();
                return $this->redirectToRoute("category_list");
            }
    }

    /**
     * @Route("/category/delete", name="category_delete")
     * @param Request $request
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getEntityManager();
        $category=$em->getRepository(entityCategory::class)->findById($request->get('id'));
        foreach($category as $cat){
            $em->remove($cat);
            $em->flush();
        }
        return new Response(json_encode([$request->get('id')]), 200, ['Content-Type' => 'application/json'] );
    }
}
