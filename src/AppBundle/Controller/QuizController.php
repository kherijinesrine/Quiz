<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Question as entityQuestion;
use AppBundle\Entity\Quiz as entityQuiz;
use AppBundle\Entity\Proposition as entityProposition;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;
use AppBundle\Form\QuizType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DateTime;

class QuizController extends Controller
{


    /**
     * @Route("/quiz/list", name="quiz_list" )
     */
    public function listAction(){
        $repository=$this->getDoctrine()->getRepository(entityQuiz::class);
        return $this->render('AppBundle:quiz:list.html.twig',
            ['repository'=>$repository]
        );
    }

    /**
     * @Route("/quiz/add", name="quiz_add" )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addAction(Request $request) {

        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository(Category::class);
        $quiz=new entityQuiz();
        $form=$this->createForm(QuizType::class,$quiz);
        $form->handleRequest($request);
        if($form->isSubmitted()) {
            $realTime='0'.$form->all()['realTime']->getViewData()['hour'].':'.'0'.$form->all()['realTime']->getViewData()['minute'].':'.'0'.$form->all()['realTime']->getViewData()['second'];
            $quiz->setRealTime($realTime);
            $quiz->setCategory($repository->find($form->all()['category']->getViewData()));
            $em->persist($quiz);

             for ($i=1;$i<=20;$i++){
                if($request->request->all()['question_'.$i]!=""){
                    $question=new entityQuestion();
                    $questionTime=isset($request->request->all()['appt_time_'.$i])?$request->request->all()['appt_time_'.$i].':00':'00:00:00';
                    $question->setTxt($request->request->all()['question_'.$i]);
                    $question->setQuestionTime($questionTime);
                    $question->setChoiceResponse($request->request->all()['type_'.$i]);
                    $question->setQuiz($quiz);
                    $em->persist($question);

                    for($k=1;$k<=6;$k++){
                        if($request->request->all()['response_'.$i.'_'.$k]!=""){
                            $proposition=new entityProposition();
                            $proposition->setTxt($request->request->all()['response_'.$i.'_'.$k]);
                            $proposition->setQuestion($question);
                            if($request->request->all()['type_'.$i]==0){
                                $status=$request->request->all()['unique_response_'.$i]==explode('_','response_'.$i.'_'.$k)[2]?true:false;
                               // $key=$request->request->all()['unique_response_'.$i]==explode('_','response_'.$i.'_'.$k)[2]?true:false;
                                $proposition->setResponse($status);
                                if($request->request->all()['unique_response_'.$i]==explode('_','response_'.$i.'_'.$k)[2]){
                                    $key=$request->request->all()['unique_response_'.$i]-1;
                                 //   dump($request->request->all()['note_'.$i][$key]);
                                    $proposition->setNote($request->request->all()['note_'.$i][$key]);
                                }

                            }

                            else{
                                $proposition->setResponse(isset($request->request->all()['multiple_response_'.$i.'_'.$k]));
                             //   dump(isset($request->request->all()['multiple_response_'.$i.'_'.$k]));
                                if(isset($request->request->all()['multiple_response_'.$i.'_'.$k])==true){
                                    $proposition->setNote($request->request->all()['note_'.$i][$k-1]);
                                }
                            }
                            $em->persist($proposition);
                        }
                    }
                }

             }

            // die();

            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                'Ajouter avec succès.'
            );
          return $this->redirectToRoute('quiz_add');
        }
        return $this->render('AppBundle:Quiz:add.html.twig',['form'=>$form->createView()]);

    }

    /**
     * @Route("/quiz/{id}/update", name="quiz_update" )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */

    public function updateAction(Request $request,$id){

        $em=$this->getDoctrine()->getManager();
        $quiz=$em->getRepository(entityQuiz::class)->find($id);
        $repository=$em->getRepository(Category::class);
        $repProposition=$em->getRepository(entityProposition::class);
        $quiz->setRealTime(new \DateTime($quiz->getRealtime()));
        $form=$this->createForm(QuizType::class,$quiz);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            $em->remove($quiz);
            $data=$request->request->all();
            $quiz=new entityQuiz();
            $realTime='0'.$form->all()['realTime']->getViewData()['hour'].':'.'0'.$form->all()['realTime']->getViewData()['minute'].':'.'0'.$form->all()['realTime']->getViewData()['second'];
            $quiz->setRealTime($realTime);
            $quiz->setName($form->all()['name']->getViewData());
            $quiz->setCategory($repository->find($form->all()['category']->getViewData()));
            $em->persist($quiz);
            for ($i=1;$i<=20;$i++){
                if($request->request->all()['question_'.$i]!=""){
                    $question=new entityQuestion();
                    $questionTime=isset($request->request->all()['appt_time_'.$i])?$request->request->all()['appt_time_'.$i].':00':'00:00:00';
                    $question->setTxt($request->request->all()['question_'.$i]);
                    $question->setQuestionTime($questionTime);
                    $question->setChoiceResponse($request->request->all()['type_'.$i]);
                    $question->setQuiz($quiz);
                    $em->persist($question);
                    for($k=1;$k<=6;$k++){
                        if($request->request->all()['response_'.$i.'_'.$k]!=""){
                            $proposition=new entityProposition();
                            $proposition->setTxt($request->request->all()['response_'.$i.'_'.$k]);
                            $proposition->setQuestion($question);
                            if($request->request->all()['type_'.$i]==0){
                                $status=$request->request->all()['unique_response_'.$i]==explode('_','response_'.$i.'_'.$k)[2]?true:false;
                                $proposition->setResponse($status);
                                if($request->request->all()['unique_response_'.$i]==explode('_','response_'.$i.'_'.$k)[2]){
                                    $key=$request->request->all()['unique_response_'.$i]-1;
                                    $proposition->setNote($request->request->all()['note_'.$i][$key]);
                                }
                            }
                            else{
                                $proposition->setResponse(isset($request->request->all()['multiple_response_'.$i.'_'.$k]));
                                if(isset($request->request->all()['multiple_response_'.$i.'_'.$k])==true){
                                    $proposition->setNote($request->request->all()['note_'.$i][$k-1]);
                                }
                            }
                            $em->persist($proposition);
                        }
                    }
                }

            }

            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'notice',
                'Modification avec succès.'
            );
            return $this->redirectToRoute("quiz_update",['id'=>$quiz->getId()]);
        }
        return $this->render('AppBundle:Quiz:update.html.twig',['form'=>$form->createView(),'quiz'=>$quiz]);
    }


    /**
     * @Route("/quiz/{id}/details", name="quiz_details")
     * @param Request $request
     * @return Response
     */
    public function details(Request $request,$id){
        $repository=$this->getDoctrine()->getRepository(entityQuiz::class);
        return $this->render('AppBundle:Quiz:details.html.twig',['repository'=>$repository,'id'=>$id]);

    }

    /**
     * @Route("/quiz/delete", name="quiz_delete")
     * @param Request $request
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getEntityManager();
        $quiz=$em->getRepository(entityQuiz::class)->findById($request->get('id'));
        foreach($quiz as $q){
            $em->remove($q);
            $em->flush();
        }
        return new Response(json_encode([$request->get('id')]), 200, ['Content-Type' => 'application/json'] );
    }

    /**
     * @Route("/question/delete", name="question_delete")
     * @param Request $request
     * @return Response
     */
    public function deleteQuestionAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getEntityManager();
        $question=$em->getRepository(entityQuestion::class)->findById($request->get('id'));
        foreach($question as $q){
            $em->remove($q);
            $em->flush();
        }
        return new Response(json_encode([$request->get('id')]), 200, ['Content-Type' => 'application/json'] );
    }

}
