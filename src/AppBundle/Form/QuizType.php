<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Category;
use UserBundle\Repository\UserRepository;
use AppBundle\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class QuizType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,['label'=>'Name (*)','attr'=>['class'=>'form-control']])
            ->add('category', EntityType::class, ['label'=>'Category(*)','class' =>Category::class ,
                    'query_builder' => function (CategoryRepository $er) {  $er->getCategories();},
                    'choice_label' => 'name','attr'=>['class' => 'form-control']]
            )
            ->add('realTime', TimeType::class, ['label'=>'Temps (*)' ,'input'  => 'datetime','widget' => 'choice','with_seconds' => true])
            ->add('submit', SubmitType::class, ['label' => '+ Ajouter','attr' => ['class' => 'btn btn-success pull-right']])
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Quiz'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_quiz';
    }


}
