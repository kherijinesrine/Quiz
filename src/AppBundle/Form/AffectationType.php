<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Quiz;
use AppBundle\Repository\QuizRepository;

class AffectationType extends AbstractType
{



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('quiz', EntityType::class, ['label'=>'Category(*)','class' =>Quiz::class ,
            //        'query_builder' => function (QuizRepository $er) {  $er->findAll();},
           //         'choice_label' => 'name','attr'=>['class' => 'form-control']]
          //  )
            ->add('submit', SubmitType::class, ['label' => '+ Ajouter','attr' => ['class' => 'btn btn-success pull-right']])
        ;
    }


/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Affectation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_affectation';
    }


}
