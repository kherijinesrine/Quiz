<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="Quiz", mappedBy="category",cascade={"persist","remove"})
     */
    private  $quiz;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
  


    /**
     * Add quiz
     *
     * @param \AppBundle\Entity\Quiz $quiz
     *
     * @return Category
     */
    public function addQuiz(\AppBundle\Entity\Quiz $quiz)
    {
        $this->quiz[] = $quiz;

        return $this;
    }

    /**
     * Remove quiz
     *
     * @param \AppBundle\Entity\Quiz $quiz
     */
    public function removeQuiz(\AppBundle\Entity\Quiz $quiz)
    {
        $this->quiz->removeElement($quiz);
    }

    /**
     * Get quiz
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuiz()
    {
        return $this->quiz;
    }
}
