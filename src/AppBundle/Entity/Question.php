<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="txt", type="text")
     */
    private $txt;

    /**
     * @var string
     *
     * @ORM\Column(name="question_time", type="string", length=255)
     */
    private $questionTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="choice_response", type="boolean")
     */
    private $choiceResponse;

    /**
     * @ORM\ManyToOne(targetEntity="Quiz", inversedBy="question" )
     */

    private $quiz;


    /**
     * @ORM\OneToMany(targetEntity="Proposition", mappedBy="question",cascade={"persist","remove"})
     */
    private  $proposition;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set txt
     *
     * @param string $txt
     *
     * @return Question
     */
    public function setTxt($txt)
    {
        $this->txt = $txt;

        return $this;
    }

    /**
     * Get txt
     *
     * @return string
     */
    public function getTxt()
    {
        return $this->txt;
    }


    /**
     * Set choiceResponse
     *
     * @param boolean $choiceResponse
     *
     * @return Question
     */
    public function setChoiceResponse($choiceResponse)
    {
        $this->choiceResponse = $choiceResponse;

        return $this;
    }

    /**
     * Get choiceResponse
     *
     * @return boolean
     */
    public function getChoiceResponse()
    {
        return $this->choiceResponse;
    }

    /**
     * Set questionTime
     *
     * @param string $questionTime
     *
     * @return Question
     */
    public function setQuestionTime($questionTime)
    {
        $this->questionTime = $questionTime;

        return $this;
    }

    /**
     * Get questionTime
     *
     * @return string
     */
    public function getQuestionTime()
    {
        return $this->questionTime;
    }

    /**
     * Set quiz
     *
     * @param \AppBundle\Entity\Quiz $quiz
     *
     * @return Question
     */
    public function setQuiz(\AppBundle\Entity\Quiz $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz
     *
     * @return \AppBundle\Entity\Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proposition = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add proposition
     *
     * @param \AppBundle\Entity\Proposition $proposition
     *
     * @return Question
     */
    public function addProposition(\AppBundle\Entity\Proposition $proposition)
    {
        $this->proposition[] = $proposition;

        return $this;
    }

    /**
     * Remove proposition
     *
     * @param \AppBundle\Entity\Proposition $proposition
     */
    public function removeProposition(\AppBundle\Entity\Proposition $proposition)
    {
        $this->proposition->removeElement($proposition);
    }

    /**
     * Get proposition
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProposition()
    {
        return $this->proposition;
    }
}
