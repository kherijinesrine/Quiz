<?php
/**
 * @copyright  Copyright (c) 2009-2014 Steven TITREN - www.webaki.com
 * @package    Webaki\UserBundle\Redirection
 * @author     Steven Titren <contact@webaki.com>
 */
namespace UserBundle\Service;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class AfterLogoutRedirection extends Controller implements LogoutSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    private $security;
    /**
     * @param SecurityContextInterface $security
     */
    public function __construct(RouterInterface $router ,$container=null ,SecurityContextInterface $security  )
    {
        $this->router = $router;
        $this->security = $security;
        $this->container = $container;
    }
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request)
    {

        $em=$this->get('doctrine')->getEntitymanager();

////recuperer USER courant
//        $user_courant=$this->getUser();
////Achaue fois aue USER connect le champ connected se modifie par "0"
//        $user_courant->setConnected(0);
//        $em->persist($user_courant);
////Ici On va recuper l'heure Courant puis on va le setter
//        $time = new \DateTime();
//        //$time->setTimezone(1);
//        $timeLog=$time->setTime(date('H'), date('i'), date('s'));
//
////Instanciation Objet Log
//        $log=new Log();
//        $log->setIdModule(0);
//        $log->setDateLog(new \DateTime());
//        $log->setModule("");
//        $log->setUser($user_courant);
//        $log->setMotif('LOGOUT');
//        $log->setTimeLog($timeLog);
//
//        $em->persist($log);
//        $em->flush();





        // Get list of roles for current user
        $roles = $this->security->getToken()->getRoles();
        // Tranform this list in array
        $rolesTab = array_map(function($role){
            return $role->getRole();
        }, $roles);
        // If is a commercial user or admin or super admin we redirect to the login area. Here we used FoseUserBundle bundle
        //  if (in_array('ROLE_COMMERCIAL', $rolesTab, true) || in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_SUPER_ADMIN', $rolesTab, true))
        // $response = new RedirectResponse($this->router->generate('fos_user_security_login'));
        // otherwise we redirect user to the homepage of website
        // else

        if (in_array('ROLE_AGENT', $rolesTab, true)|| in_array('ROLE_HOST', $rolesTab, true) ) {
            $response = new RedirectResponse($this->router->generate('gestion_front_homepage'));
        }
        elseif(in_array('ROLE_SUPER_ADMIN', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('fos_user_security_login'));

        }
        return $response;
    }
}