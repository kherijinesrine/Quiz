<?php
namespace UserBundle\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class AfterLoginRedirection extends Controller implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, $container = null)
    {
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {


        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $token->getRoles());

        if (in_array('ROLE_USER', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('homepage'));
        }

        return $redirection;
    }
}