<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class UserType extends AbstractType
{

    protected  function getRoles(){

        return [
            'admin'=>'ROLE_ADMIN',
            'moderator'=>'ROLE_MODERATOR',
            'candidator'=>'ROLE_CANDIDATOR'
        ];
    }
    protected  function activate(){

        return [
            'Non'=>0,
            'Oui'=>1

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('brochure', FileType::class,['data_class'=>null,'label'=>'CV (*)','attr' => ['accept' => '.csv,.txt,.pdf']])
            ->add('firstname',TextType::class,['label'=>'Prénom (*)','attr'=>['class'=>'form-control']])
            ->add('lastname',TextType::class,['label'=>'Nom (*)','attr'=>['class'=>'form-control']])
            ->add('phone',NumberType::class,['invalid_message' => 'Cette valeur n\'est pas valide.','label'=>'Téléphone (*)','attr'=>['class'=>'form-control']])
            ->add('email',EmailType::class,['label'=>'Email (*)','attr'=>['class'=>'form-control']])
            ->add('enabled',ChoiceType::class,['label'=>'Active (*)','choices'=> $this->activate(),'expanded'=>true])
            ->add('roles', ChoiceType::class, ['label' => 'Roles (*)','choices'=> $this->getRoles(),'multiple'=>true,'attr'=>['class'=>'form-control']])
            ->add('submit', SubmitType::class, ['label' => '+ Ajouter','attr' => ['class' => 'btn btn-success pull-right']])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les champs mot de passe doivent correspondre.',
                'options' => ['attr' => ['class' => 'form-control']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe(*)'],
                'second_options' => ['label' => 'Confirmer mot de passe(*)'],
            ])
            // ->add('password_requested_at',PasswordType::class)
            //  ->add('password',PasswordType::class)
           //->add('file','file')

        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_user';
    }


}
